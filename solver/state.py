import numpy as np
import pandas as pd
from tabulate import tabulate


class State:
    def __init__(self, pstatus, cars_a, cars_b, possDecs):
        self.balance = 0
        self.cars_a = cars_a
        self.cars_b = cars_b
        self.pstatus = pstatus
        self.tmatrix1 = [[0 for x in range(pstatus)] for y in range(pstatus)]
        self.tmatrix2 = [[0 for x in range(pstatus)] for y in range(pstatus)]
        self.pmatrix = [[0 for x in range(pstatus)] for y in range(2)]
        self.decmatrix = [[0 for x in range(pstatus)] for y in range(pstatus)]
        self.decmatrixIter = [[0 for x in range(pstatus)] for y in range(pstatus)]
        self.possDecs = possDecs
        self.IniQmatrix = [
            [-999 for x in range(pstatus * pstatus)] for x in range(possDecs)
        ]
        self.Qmatrix = [
            [-999 for x in range(pstatus * pstatus)] for x in range(possDecs)
        ]
        self.Pmatrix = [
            [[0 for x in range(pstatus * pstatus)] for y in range(pstatus * pstatus)]
            for x in range(possDecs)
        ]
        # 0->(0,0) 1->(-1,1) 2->(-2,2) 3->(-3,3) 4->(1,-1) 5->(2,-2) 6->(3,-3)
        self.prevFmatrix = [0 for x in range(pstatus * pstatus)]
        self.currFmatrix = [0 for x in range(pstatus * pstatus)]
        self.Dmatrix = [0 for x in range(pstatus * pstatus)]

    def calc_prob(self, i, j, filial, filialID):
        ret = 0
        carProfit = 30
        pedidos = filial["Pedidos"]
        entregas = filial["Entregas"]

        probFullBuy = 0
        for k in range(i, self.pstatus):
            probFullBuy += pedidos[k]

        ret += probFullBuy * entregas[j]
        self.pmatrix[filialID][i] += probFullBuy * entregas[j] * carProfit * i

        if j == 0:
            ret += 0

        elif j == (self.pstatus - 1):
            min_pedidos = 0
            max_pedidos = i
            for k in range(min_pedidos, max_pedidos):
                carStock = i - k
                min_entregas = self.pstatus - carStock - 1
                max_entregas = self.pstatus
                for z in range(min_entregas, max_entregas):
                    ret += pedidos[k] * entregas[z]
                    self.pmatrix[filialID][i] += (
                        pedidos[k] * entregas[z] * carProfit * k
                    )

        else:
            if j > i:
                diferenca = j - i
                for k in range(0, i):
                    ret += pedidos[k] * entregas[k + diferenca]
                    self.pmatrix[filialID][i] += (
                        pedidos[k] * entregas[k + diferenca] * carProfit * k
                    )

            else:
                diferenca = i - j
                for k in range(diferenca, i):
                    ret += pedidos[k] * entregas[k - diferenca]
                    self.pmatrix[filialID][i] += (
                        pedidos[k] * entregas[k - diferenca] * carProfit * k
                    )

        return ret

    def initTransitionMatrixes(self, filial1, filial2):
        for i in range(self.pstatus):
            for j in range(self.pstatus):
                self.tmatrix1[i][j] = self.calc_prob(i, j, filial1, 0)
                self.tmatrix2[i][j] = self.calc_prob(i, j, filial2, 1)

    def calcCustoParque(self, carsA, carsB):
        custoPcarro = 10
        if self.pstatus == 13:
            carLimit = 8
        else:
            carLimit = 1

        custoA = custoB = 0
        if carsA > carLimit:
            custoA = 10
        if carsB > carLimit:
            custoB = 10

        return custoA + custoB

    def calcdecs(self):
        custoTransferPcarro = 7

        for i in range(self.pstatus):
            for j in range(self.pstatus):
                lucro = -10000
                maxAB = min(3, i, self.pstatus - 1 - j)
                maxBA = min(3, j, self.pstatus - 1 - i)

                for AtoB in range(maxAB + 1):
                    newcarsA = i - AtoB
                    newcarsB = j + AtoB
                    custoParque = self.calcCustoParque(newcarsA, newcarsB)
                    custoTransfer = AtoB * custoTransferPcarro
                    lucroSales = self.pmatrix[0][newcarsA] + self.pmatrix[1][newcarsB]

                    totalDia = lucroSales - custoParque - custoTransfer
                    if totalDia > lucro:
                        self.decmatrix[i][j] = [-AtoB, AtoB]
                        lucro = totalDia

                for BtoA in range(maxBA + 1):
                    newcarsA = i + BtoA
                    newcarsB = j - BtoA
                    custoParque = self.calcCustoParque(newcarsA, newcarsB)
                    custoTransfer = BtoA * custoTransferPcarro
                    lucroSales = self.pmatrix[0][newcarsA] + self.pmatrix[1][newcarsB]

                    totalDia = lucroSales - custoParque - custoTransfer
                    if totalDia > lucro:
                        self.decmatrix[i][j] = [BtoA, -BtoA]
                        lucro = totalDia

                if lucro == (-10000):
                    newcarsA = i
                    newcarsB = j
                    custoParque = self.calcCustoParque(newcarsA, newcarsB)
                    custoTransfer = 0
                    lucroSales = self.pmatrix[0][newcarsA] + self.pmatrix[1][newcarsB]
                    totalDia = lucroSales - custoParque - custoTransfer
                    self.decmatrix[i][j] = [0, 0]

    def fillPs(self):
        # inicial A
        for i in range(self.pstatus):
            # inicial B
            for j in range(self.pstatus):
                # final A
                for k in range(self.pstatus):
                    # final B
                    for z in range(self.pstatus):
                        # 0->(0,0)
                        self.Pmatrix[0][i * 13 + j][k * 13 + z] = (
                            self.tmatrix1[i][k] * self.tmatrix2[j][z]
                        )
                        # 1->(-1,1)
                        if i >= 1 and j <= 11:
                            self.Pmatrix[1][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i - 1][k] * self.tmatrix2[j + 1][z]
                            )
                        # 2->(-2,2)
                        if i >= 2 and j <= 10:
                            self.Pmatrix[2][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i - 2][k] * self.tmatrix2[j + 2][z]
                            )
                        # 3->(-3,3)
                        if i >= 3 and j <= 9:
                            self.Pmatrix[3][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i - 3][k] * self.tmatrix2[j + 3][z]
                            )
                        # 4->(1,-1)
                        if j >= 1 and i <= 11:
                            self.Pmatrix[4][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i + 1][k] * self.tmatrix2[j - 1][z]
                            )
                        # 5->(2,-2)
                        if j >= 2 and i <= 10:
                            self.Pmatrix[5][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i + 2][k] * self.tmatrix2[j - 3][z]
                            )
                        # 6->(3,-3)
                        if j >= 3 and i <= 9:
                            self.Pmatrix[6][i * 13 + j][k * 13 + z] = (
                                self.tmatrix1[i + 3][k] * self.tmatrix2[j - 3][z]
                            )

    def fillQs(self):
        custoTransferPcarro = 7

        for i in range(self.pstatus):
            for j in range(self.pstatus):

                maxAB = min(3, i, self.pstatus - 1 - j)
                maxBA = min(3, j, self.pstatus - 1 - i)

                for AtoB in range(1, maxAB + 1):
                    newcarsA = i - AtoB
                    newcarsB = j + AtoB
                    custoParque = self.calcCustoParque(newcarsA, newcarsB)
                    custoTransfer = AtoB * custoTransferPcarro
                    lucroSales = self.pmatrix[0][newcarsA] + self.pmatrix[1][newcarsB]

                    totalDia = lucroSales - custoParque - custoTransfer
                    self.IniQmatrix[AtoB][i * 13 + j] = totalDia

                for BtoA in range(1, maxBA + 1):
                    newcarsA = i + BtoA
                    newcarsB = j - BtoA
                    custoParque = self.calcCustoParque(newcarsA, newcarsB)
                    custoTransfer = BtoA * custoTransferPcarro
                    lucroSales = self.pmatrix[0][newcarsA] + self.pmatrix[1][newcarsB]

                    totalDia = lucroSales - custoParque - custoTransfer
                    self.IniQmatrix[BtoA + 3][i * 13 + j] = totalDia

                custoParque = self.calcCustoParque(i, j)
                custoTransfer = 0
                lucroSales = self.pmatrix[0][i] + self.pmatrix[1][j]
                totalDia = lucroSales - custoParque - custoTransfer
                self.IniQmatrix[0][i * 13 + j] = totalDia

    def calcGain(self, index):
        total = 0
        for i in range(self.pstatus * self.pstatus):
            total += (
                self.currFmatrix[i]
                * self.Pmatrix[self.decmatrixIter[index // 13][index % 13]][index][i]
            )
        return total

    def calcGainSim(self, index, currFmatrix, decmatrix):
        total = 0
        for i in range(self.pstatus * self.pstatus):
            total += (
                currFmatrix[i]
                * self.Pmatrix[decmatrix[index // 13][index % 13]][index][i]
            )
        return total

    def pickBestGain(self, index):
        bestDec = -999
        bestDecindex = 0
        for i in range(self.possDecs):
            if self.Qmatrix[i][index] > bestDec:
                bestDec = self.Qmatrix[i][index]
                bestDecindex = i

        return bestDec, bestDecindex

    def calcDecsIter(self):
        FgainVector = [0 for x in range(self.pstatus * self.pstatus)]

        # Calcular ganhos Fn, adicionar as matrizes Q
        for i in range(self.pstatus * self.pstatus):
            FgainVector[i] = self.calcGain(i)
            for j in range(7):
                self.Qmatrix[j][i] = self.IniQmatrix[j][i] + FgainVector[i]

        # Susbstituir Fn-1 por Fn
        for i in range(self.pstatus * self.pstatus):
            self.prevFmatrix[i] = self.currFmatrix[i]

        # Escolher o maior ganho de entre as matrizes Q para um determinado estado,
        # colocá-lo em Fn e colocar a decisão em decMatrixIter
        for i in range(self.pstatus * self.pstatus):
            gain, index = self.pickBestGain(i)
            self.currFmatrix[i] = gain
            self.decmatrixIter[i // 13][i % 13] = index

        # Calcular Fn-(Fn-1) e colocar em D
        for i in range(self.pstatus * self.pstatus):
            self.Dmatrix[i] = self.currFmatrix[i] - self.prevFmatrix[i]

        maxGain = -999
        minGain = 999
        for i in range(self.pstatus * self.pstatus):
            if self.Dmatrix[i] > maxGain:
                maxGain = self.Dmatrix[i]
            if self.Dmatrix[i] < minGain:
                minGain = self.Dmatrix[i]

        return (maxGain - minGain) > 0.000001

    def simulIter(self, decmatrixSim):
        FgainVector = [0 for x in range(self.pstatus * self.pstatus)]
        prevFmatrix = [0 for x in range(self.pstatus * self.pstatus)]
        currFmatrix = [0 for x in range(self.pstatus * self.pstatus)]
        Dmatrix = [0 for x in range(self.pstatus * self.pstatus)]

        self.removepaintDecMatrix(decmatrixSim)

        # Calcular ganhos Fn, adcurrFicionar as matrizes Q
        notstop = True
        count = 0
        while notstop and count < 200:
            for i in range(self.pstatus * self.pstatus):

                FgainVector[i] = self.calcGainSim(i, currFmatrix, decmatrixSim)
                for j in range(7):
                    self.Qmatrix[j][i] = self.IniQmatrix[j][i] + FgainVector[i]

            # Susbstituir Fn-1 por Fn
            for i in range(self.pstatus * self.pstatus):
                prevFmatrix[i] = currFmatrix[i]

            # Escolher o ganho da matriz Q de cada decisao para um determinado estado
            for i in range(self.pstatus * self.pstatus):
                gain = self.Qmatrix[decmatrixSim[i // 13][i % 13]][i]
                currFmatrix[i] = gain

            # Calcular Fn-(Fn-1) e colocar em D
            for i in range(self.pstatus * self.pstatus):
                Dmatrix[i] = currFmatrix[i] - prevFmatrix[i]

            maxGain = -999
            minGain = 999
            for i in range(self.pstatus * self.pstatus):
                if Dmatrix[i] > maxGain:
                    maxGain = Dmatrix[i]
                if Dmatrix[i] < minGain:
                    minGain = Dmatrix[i]

            notstop = (maxGain - minGain) > 0.000001
            count += 1

        print(sum(Dmatrix) / len(Dmatrix))

    def repaintDecMatrix(self):
        decs = [[0, 0], [-1, 1], [-2, 2], [-3, 3], [1, -1], [2, -2], [3, -3]]

        for i in range(self.pstatus):
            for j in range(self.pstatus):
                self.decmatrixIter[i][j] = decs[self.decmatrixIter[i][j]]

    def removepaintDecMatrix(self, decmatrix):
        decs = [[0, 0], [-1, 1], [-2, 2], [-3, 3], [1, -1], [2, -2], [3, -3]]

        for i in range(self.pstatus):
            for j in range(self.pstatus):
                for k in range(self.possDecs):
                    if decmatrix[i][j] == decs[k]:
                        decmatrix[i][j] = k

    def iterate(self):
        dontStop = True
        while dontStop:
            dontStop = self.calcDecsIter()
        self.repaintDecMatrix()
