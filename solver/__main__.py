#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Solver.

Usage:
    solver [--log=<level>]
    solver (-h | --help)
    solver --version

Options:
    --log=<level>  Logging level [default: INFO].
"""
from docopt import docopt
import pandas as pd
import numpy as np
import logging as log
from tabulate import tabulate
from .state import State

__version__ = "0.1.0"

FILIAL1_DATA = "data/filial_1.csv"
FILIAL2_DATA = "data/filial_2.csv"

if __name__ == "__main__":
    arguments = docopt(__doc__, version=__version__)
    log.basicConfig(level=getattr(log, arguments["--log"].upper()))

    estado = State(13, 12, 12, 7)
    estadotest = State(3, 2, 2, 2)

    filial1 = pd.read_csv(FILIAL1_DATA)
    filial2 = pd.read_csv(FILIAL2_DATA)

    # Cálculo das matrizes de transição
    estado.initTransitionMatrixes(filial1, filial2)

    log.info(
        "Matriz transição filial 1\n" + tabulate(estado.tmatrix1, tablefmt="fancy_grid")
    )
    log.info(
        "Matriz transição filial 2\n" + tabulate(estado.tmatrix2, tablefmt="fancy_grid")
    )

    log.info(
        "Contribuições de estado\n" + tabulate(estado.pmatrix, tablefmt="fancy_grid")
    )

    estado.fillQs()
    estado.fillPs()
    estado.iterate()

    log.info(f"Ganho médio: {sum(estado.Dmatrix) / len(estado.Dmatrix) }\n")
    log.info(
        "Politica Ótima Geral com convergência\n"
        + tabulate(estado.decmatrixIter, tablefmt="fancy_grid")
    )

    pd.DataFrame(estado.decmatrixIter).to_csv("output/resultados.csv")

    # --- Simulação de politicas de Transição ---

    simulationMatrix = [
        [0 for x in range(estado.pstatus)] for y in range(estado.pstatus)
    ]
    for i in range(3, estado.pstatus - 3):
        for j in range(0, 12 - 3):
            simulationMatrix[i][j] = [0, 0]

    estado.simulIter(simulationMatrix)

    # --- Verificação das matrizes de transição ---
    for i in range(13):
        ret1 = 0
        ret2 = 0
        log.debug("")
        for j in range(13):
            ret1 += estado.tmatrix1[i][j]
            ret2 += estado.tmatrix2[i][j]
        log.debug(ret1)
        log.debug(ret2)

    # Exportar matrizes
    pd.DataFrame(estado.tmatrix1).to_csv("matrices/transition_A.csv")
    pd.DataFrame(estado.pmatrix[0]).to_csv("matrices/contribution_A.csv")

    pd.DataFrame(estado.tmatrix2).to_csv("matrices/transition_B.csv")
    pd.DataFrame(estado.pmatrix[1]).to_csv("matrices/contribution_B.csv")

    pd.DataFrame(estado.Pmatrix[0]).to_csv("matrices/p_matrix_0.csv")
    pd.DataFrame(estado.Pmatrix[1]).to_csv("matrices/p_matrix_1.csv")
    pd.DataFrame(estado.Pmatrix[2]).to_csv("matrices/p_matrix_2.csv")
    pd.DataFrame(estado.Pmatrix[3]).to_csv("matrices/p_matrix_3.csv")
    pd.DataFrame(estado.Pmatrix[4]).to_csv("matrices/p_matrix_4.csv")
    pd.DataFrame(estado.Pmatrix[5]).to_csv("matrices/p_matrix_5.csv")
    pd.DataFrame(estado.Pmatrix[6]).to_csv("matrices/p_matrix_6.csv")

    pd.DataFrame(estado.IniQmatrix[0]).to_csv("matrices/q_matrix_0.csv")
    pd.DataFrame(estado.IniQmatrix[1]).to_csv("matrices/q_matrix_1.csv")
    pd.DataFrame(estado.IniQmatrix[2]).to_csv("matrices/q_matrix_2.csv")
    pd.DataFrame(estado.IniQmatrix[3]).to_csv("matrices/q_matrix_3.csv")
    pd.DataFrame(estado.IniQmatrix[4]).to_csv("matrices/q_matrix_4.csv")
    pd.DataFrame(estado.IniQmatrix[5]).to_csv("matrices/q_matrix_5.csv")
    pd.DataFrame(estado.IniQmatrix[6]).to_csv("matrices/q_matrix_6.csv")
