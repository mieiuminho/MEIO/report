\part{}

# Introdução

O exercício 1 corresponde a um problema de programação dinâmica com estados
ilimitados e alternativas, tendo como objetivo a implementação do algoritmo de
iteração de valor para resolução de tais problemas. A implementação do algoritmo
tem como pré-requisito o conhecimento sobre as matrizes de transição e
contribuição de estado que temos de provisoriamente calcular.

# Descrição do exercício

O exercício consiste num problema de gerência de duas filiais de automóveis
para aluguer geridas pelo mesmo empresário, cada filial pode receber um número
variado de pedidos e entregas diariamente, sendo que os automóveis recebidos
durante o dia não podem ser alugados no próprio. O empresário é creditado 30€
por cada carro alugado e, no fim de cada dia tem a possibilidade de transferir
até três carros entre filiais ao custo de 7€ por transferência. As filiais têm
capacidade até 12 carros mas se no final do dia estiverem mais de 8 carros numa
filial o empresário tem de pagar um custo de parque de 10€ nessa mesma filial.

# Ponderação do problema

Dentro do problema apresentado, consideramos que um estado corresponde a um
tuplo (0-12,0-12) que corresponde à quantidade de carros presentes em cada
filial. Um estágio corresponde ao momento no final do dia em que o empresário
decide quantos carros pretende transferir. A contribuição de estado
correspondente ao valor esperado de vendas no dia seguinte de acordo com os
novos estados subtraído dos custos de transferência e dos gastos com parque
durante a noite.

## Esboço de rede

![Exemplo de rede](figures/rede.png)

Na figura acima podemos ver um exemplo muito simplificado da rede do problema em
questão( Filiais com máximo de 1 carro, sem custo parque, mas em que o ganho
expectável por carro em B é superior e a única possibilidade de transferência é
1 veiculo de A para B), podemos ver que ao realizar uma transferência o ganho é
afetado mas subtraído do custo de transferência e as possibilidades e
probabilidades de transição são afetadas por esta.

## Formulação das Matrizes de transição

Implementamos um algoritmo em _Python_ para a geração das matrizes de transição
para cada filial, tivemos de ter em atenção que uma filial não pode durante o
dia alugar mais carros de que os que tinha no início do dia, mesmo que receba
entregas durante o dia nem pode exceder o limite de 12 carros, mesmo que receba
mais entregas.

## Validação das Matrizes de transição

Verificamos que as matrizes de transição foram bem calculadas somando todas as
probabilidades de um estado individual para qualquer outro e verificando que
todos davam um total de 1.

## Formulação das Matrizes de Lucro de Vendas

Para a geração das matrizes de lucro de vendas aproveitamos o algoritmo
anterior, e sempre que adicionávamos uma probabilidade de transição entre
estados, adicionávamos também o lucro de venda gerado por essa transição
multiplicado pela probabilidade dessa transição ocorrer.

## Validação das Matrizes de Lucro de Vendas

Para a validação das matrizes de lucro de vendas criamos duas filiais de teste
que cujos ganhos conseguimos calcular manualmente e verificamos que coincidiam,
dai concluirmos que o mesmo método funciona também para filiais com mais estados
possíveis.

# Cálculo da Política Ótima

## Método Iterativo

Para a decisão da política implementamos o algoritmo para Numero infinito de
Estados com alternativas, para isto precisamos de calcular as matrizes $Q_{k}$ e
$P_{k}$ e implementar a iteração individual.

### Matrizes $Q_{k}$

As matrizes $Q_{k}$ que correspondem aos ganhos esperados para cada estado para
cada decisão tomada (No caso em que a decisão é impossível decidimos que o ganho
esperado é -999 para que nunca fosse considerada).

#### Matriz $P_{k}$

A matrizes $P_{k}$ [169 x 169] corresponde à possibilidade de transitar de cada
par de veículos (`carsA`, `carsB`) nas filiais para qualquer outro, tal como nas
transições individuais nas filiais, como cada decisão influencia o estado atual
de uma maneira diferente, tivemos de calcular uma matriz de transição para cada
estado que reflita essa mesma alteração, no caso de uma decisão para não ser
possível para certo estado inicial ser, essa linha de transição fica a 0, isto
não tem implicação na resolução do problema já que essa linha nunca será
utilizada para calcular ganho, já que nunca de tomará essa decisão para um
estado impossível (O seu ganho é -999). Verificamos que o calculo esta correto
somando todas as transições de cada estado relevante e verificando que todas
davam um total de 1.

### Iteração

Para implementar uma iteração seguimos os passos presentes no algoritmo presente
nos slides da disciplina:

1 - Calcular os ganhos $F_{n}$ e adicioná-los às matrizes $Q_{k}$.
2 - Substituir $F_{n-1}$ por $F_{n}$.
3 - Escolher a melhor decisão para cada estado, preencher $F_{n}$ e guardar a
decisão tomada.
4 - Calcular $D_{n}$ através de $F_{n} - F_{n-1}$.
5 - Verificar de o critério de paragem se verifica, isto é, a diferença entre o
maior ($U_{n}$) e o menor ($L_{n}$) valor de ganho em $D_{n}$ é menor que um
certo valor definido, neste caso escolhemos: `0.0000001`.

## Iterar

Por fim, basta apenas correr iteração até que o critério de paragem se verifique
já que o método garante a convergência para matrizes $Q_{k}$ e $P_{k}$ fixas,
colocamos um limite de 200 iterações para que o programa não corra
indefinidamente se existir algum erro nas probabilidades

# Resultados

Depois de corrermos o algoritmo verificámos que o ganho diário médio expectável
é 178.02€.

Na tabela seguinte podemos verificar qual é a política ótima de transferência.

|     | 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   |
| --- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 0   | 0,0  | 0,0  | 0,0  | 0,0  | 1,-1 | 1,-1 | 1,-1 | 2,-2 | 2,-2 | 2,-2 | 2,-2 | 3,-3 | 3,-3 |
| 1   | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 1,-1 | 1,-1 | 1,-1 | 1,-1 | 2,-2 | 3,-3 | 2,-2 |
| 2   | -1,1 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 1,-1 | 2,-2 | 1,-1 | 1,-1 |
| 3   | -2,2 | -1,1 | -1,1 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 1,-1 | 0,0  | 0,0  | 0,0  |
| 4   | -2,2 | -2,2 | -2,2 | -1,1 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 1,-1 | 0,0  | 0,0  | 0,0  |
| 5   | -3,3 | -3,3 | -2,2 | -2,2 | -1,1 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 6   | -3,3 | -3,3 | -3,3 | -2,2 | -2,2 | -1,1 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 7   | -3,3 | -3,3 | -3,3 | -3,3 | -2,2 | -2,2 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 8   | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -2,2 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 9   | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -2,2 | -1,1 | -1,1 | 0,0  | -1,1 | -1,1 | -1,1 | 0,0  |
| 10  | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -2,2 | -2,2 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 11  | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |
| 12  | -3,3 | -3,3 | -3,3 | -3,3 | -3,3 | -2,2 | -1,1 | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  | 0,0  |

: Política ótima gerada pelo programa.

As linhas correspondem aos número de veículos em A, as colunas aos presentes em
B, e o par de valores corresponde à diferença de veículos no fim da transição,
por exemplo (-1,1) corresponde a transferir um veículo de A para B.

# Análise dos resultados

Para verificar que o resultado correspondia à política ótima de transferências
criamos uma função que simula o algoritmo de iteração de valor mas com decisões
fixas de modo a observarmos se alguma política de transferência poderia de facto
produzir um resultado melhor, testamos várias políticas, em particular
verificamos que se o empresário nunca transferisse veículos o ganho médio diário
seria de 167.78€, isto significa que a a adoção da política ótima de
transferências permite que o empresário aumente os seus lucros diários em 10.24€
comparativamente à opção de não transferir nenhum veículo. De entre todas as
políticas alternativas que verificamos, nenhuma permitiu obter um valor superior
à anterior, e por isso aceitamos esta como ideal.

# Conclusão

De um modo geral, conseguimos fazer uma implementação funcional ainda específica
ao problema em causa do algoritmo de iteração de valor que nos permitiu obter
uma solução que validámos como sendo ótima para o problema em causa, e um
simulador de iteração que nos permitiu verificar o ganho expectável se
adotássemos qualquer outra política de transferência entre as filiais.


\setcounter{section}{0}
\renewcommand\thesection{\Alph{section}}

\anexos

Os anexos podem ser encontrados em <https://we.tl/t-GceDzh5wwM>.

# Dados Fornecidos

1. `data/meio_tp1_g06.dat`

# Matrizes de Transição

**Matrizes de transição indivídual**

1. `matrices/transition_A.csv`
2. `matrices/transition_B.csv`

**Matrizes de ganho indivídual**

1. `matrices/contribution_A.csv`
2. `matrices/contribution_B.csv`

**Matrizes de transição conjunta para cada decisão**

1. `matrices/p_matrix_0.csv`
2. `matrices/p_matrix_1.csv`
3. `matrices/p_matrix_2.csv`
4. `matrices/p_matrix_3.csv`
5. `matrices/p_matrix_4.csv`
6. `matrices/p_matrix_5.csv`
7. `matrices/p_matrix_6.csv`

**Matrizes de ganho conjunto para cada decisão**

1. `matrices/q_matrix_0.csv`
2. `matrices/q_matrix_1.csv`
3. `matrices/q_matrix_2.csv`
4. `matrices/q_matrix_3.csv`
5. `matrices/q_matrix_4.csv`
6. `matrices/q_matrix_5.csv`
7. `matrices/q_matrix_6.csv`

# Código do Solver

1. `solver/__main__.py`
2. `solver/state.py`

# Inputs

1. `data/filial_1.csv`
2. `data/filial_2.csv`

# Outputs

1. `output/resultados.csv`

\renewcommand\thesection{\arabic{section}}

