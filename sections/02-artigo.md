\newpage
\part{}

O artigo escolhido intitula-se "_Using markov chains to forecast the proportion
of noncommunicable diseases_" [@boonya2019].

# Obtenção do modelo

Os tempos difíceis que vivemos tornam evidentes os perigos subjacentes às
doenças transmissíveis, como por exemplo doenças virais. No entanto existe um
outro leque de doenças que podem ser prevenidas e que reclamam, anualmente,
cerca de 40 milhões de vidas, as doenças não transmissíveis. Existem 4
principais fatores metabólicos para o desenvolvimento deste tipo de doenças:
pressão arterial elevada, excesso de peso ou obesidade, níveis de glicose no
sangue elevados e níveis elevados de gordura no sangue.

Nesta medida, encontramos um estudo para a previsão da incidência de doenças não
contagiosas na Universidade Privada da Província de Pathumthani que utiliza
cadeias Markov, pelo que decidimos analisá-lo. Existe um número de exames
correspondente a 2469, 2486 e 1618 nos passados anos de 2015, 2016 e 2017,
respetivamente. Sinais de doenças não transmissíveis traduzem-se,
essencialmente, em:

- Níveis de glicose no sangue em jejum
- Pressão arterial
- Triglicerídeos
- Colesterol
- Baixa densidade lipo-proteíca

Existe um quadro que relaciona indicadores presentes nessas análises com os
códigos que refletem as situações de anomalia.

| Critério                             | Valor registado        | Código |
| ------------------------------------ | ---------------------- | ------ |
| Níveis de glicose no sangue em jejum | > 100 mg/dL.           | 1      |
| Pressão arterial                     | >= 140`|`90 mmHg.      | 2      |
| Triglicerídeos ou                    | >= 200 mg/dL.          |        |
| Colesterol ou                        | >= 150 mg/dL.          | 3      |
| Baixa densidade lipo-proteíca        | >= 130 mg/dL.          |        |
| Eletrocardiograma                    | sinais de anormalidade | 4      |

: Relação das anomalia com o código.

\newpage

Estabelece-se também uma relação entre estágios e os códigos de anomalia
confirmados nas análises de cada paciente conforme a seguinte tabela:

| Estado | Código do Estado | Códigos confirmados |
| ------ | ---------------- | ------------------- |
| 0      | 0                | Nenhum              |
| 1      | 1                | 1                   |
| 2      | 12               | 1 e 2               |
| 3      | 123              | 1, 2 e 3            |
| 4      | 1234             | 1, 2, 3 e 4         |
| 5      | 124              | 1, 2 e 4            |
| 6      | 13               | 1 e 3               |
| 7      | 134              | 1, 3 e 4            |
| 8      | 14               | 1 e 4               |
| 9      | 2                | 2                   |
| 10     | 23               | 2 e 3               |
| 11     | 234              | 2, 3 e 4            |
| 12     | 24               | 2 e 4               |
| 13     | 3                | 3                   |
| 14     | 34               | 3 e 4               |
| 15     | 4                | 4                   |

: Relação entre o estado e os códigos de anomalias verificados.

Para aumentar o conhecimento relativamente a este tipo de doenças a universidade
procurou um método de prever o impacto das mesmas na comunidade académica. A
gerência percebeu que as cadeias Markovianas eram uma alternativa válida para
tentar prever os indicadores testados.

Os exames previamente mencionados realizados em anos anteriores têm os seguintes
campos: identificador pessoal, idade, género, altura, peso, índice de massa
corporal e os quatro critérios que constam da Tabela 1: nível de açúcar no
sangue em jejum, pressão arterial, triglicerídeos, colesterol, baixa densidade
lipo-proteíca e o eletrocardiograma.

As cadeias de Markov são um método estocástico que interpreta e descreve vários
fenómenos da mundo real que evoluem ao longo do tempo e que envolvem uma
componente estocástica aleatória. Para prever o estado evolutivo da incidência
deste tipo de doenças o **estado** deste problema são as 16 combinações da
Tabela 2. O **estágio** é anual (a previsão é feita anualmente).

Numa cadeia de Markov finita com $n$ estados, sendo que $n$ é um número inteiro
positivo tal que $n > 2$, a matriz de transição é uma matriz $n \times n$, em que
$m_{ij}$ é a entrada da linha $i$ e coluna $j$ é a probabilidade de transição do
estado $S_{i}$ para o estado $S_{j}$, sendo que $i, j = 1, 2, ..., n$. A
transição de um estado para um outro estado é um acontecimento certo pelo que a
soma dos elementos de uma qualquer linha da matriz $m$ é igual a 1.

Para fazer previsões a curto prazo para a evolução da incidência deste tipo de
doenças pode ser utilizada uma cadeia de Markov finita com $k + 1$
passos em que a probabilidade de transição é $P_{k + 1} = P_{k} \times M$ e
$P_{k + 1} = P_{0} \times M^{k}$ para todos os inteiros não negativos $k$.

Uma cadeia Markoviana é uma cadeia ergódica se é possível que, à medida que o
número de passos tende para o infinito (para previsões a longo prazo), a cadeia
tende para um situação de equilíbrio em que o vetor $P_{k}$ toma um valor
constante $P = \begin{matrix} p_{1} & p_{2} & ... & p_{n} \end{matrix}$ que é
chamado o estado estacionário. A situação de equilíbrio é $P = PM$ e $p_{1} +
p_{2} + ... + p_{n} = 1$. Com as cadeias ergódicas podem ser obtidas previsões a
longo prazo para a evolução do fenómeno em estudo.

Por forma a concretizar a aplicação das cadeias de Markov para prever a
proporção dos estados das doenças não transmissíveis foi seguido um conjunto de
passos que vamos enumerar.

1. Cada registo médico de anos anteriores é constituído pelas seguintes
   entradas: identificador pessoal, idade, sexo, altura, peso, índice de massa
   corporal, nível de açúcar no sangue em jejum,triglicerídeos, colesterol,
   baixa densidade lipo-proteíca e resultado do eletrocardiograma.

2. Cada um dos resultados dos exames é comparado com a Tabela 1 e atribuído um
   ou mais código, conforme a coluna dos valores.

3. Os códigos obtidos são concatenados obtendo-se assim uma codificação que
   corresponde a um estado conforme a codificação da Tabela 2 ($S_{i} = 0, 1, 2,
   ... 15$).

4. Calcular a frequência relativa de cada estado em cada um dos anos dos quais
   se têm registos de modo a obter a probabilidade real de cada um dos estados
   nesses anos.

5. O método de "Cross tabulating" é usado para obter as matrizes de transição de
   2015 para 2016 ($M_{2015 \rightarrow 2016}$), e de 2016 para 2017 ($M_{2016
   \rightarrow 2017}$).

6. A previsão obtida através da cadeia de Markov para 2017 é avaliada contra os
   resultados verificados em 2017, é efetuado o teste do Qui-quadrado e
   confirma-se a correlação linear das variáveis.

7. No caso deste artigo, foram calculadas as probabilidades dos estados para
   2017, 2018, 2022 e o estado limite ($P_{2017}$, $P_{2018}$, $P_{2022}$ e $P$)
   são calculadas através da matriz de transição de 2016 e 2017 ($M_{2016
   \rightarrow 2017}$).

# Conclusões

O estudo concluiu que se não forem tomadas medidas por parte da comunidade
académica e se os indivíduos mantiverem os estilos de vida a percentagem de
pessoas saudáveis diminuirá de 17% para 16%. A probabilidade do Estado 4 (código
1234), Estado 6 (código 13) e Estado 8 (código 14) em $P_{2022}$ e $P$ são
maiores relativamente a $P_{2018}$ e $P_{2019}$ em cerca de 1%. Por outro lado,
as probabilidades dos Estados 0 (código 0), 13 (código 3) e 14 (código 34) irão
decrescer em cerca de 1%.

# Bibliografia


