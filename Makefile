#==============================================================================
SHELL   = zsh
#------------------------------------------------------------------------------
FILTERS = -F pandoc-include-code
OPTIONS = --template=styles/template.tex $(FILTERS)
CONFIG  = --metadata-file config.yml
BIB     = --filter pandoc-citeproc --bibliography=references.bib
#------------------------------------------------------------------------------
SEC     = $(shell ls $(SEC_DIR)/**/*.md)
SEC_DIR = sections
REPORT  = report
#------------------------------------------------------------------------------
SRC_DIR = solver
#==============================================================================

pdf:
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SEC) -o $(REPORT).pdf

setup:
	@python3.8 -m pip install -r requirements.txt

run:
	@python3.8 -m $(SRC_DIR)

debug:
	@python3.8 -m $(SRC_DIR) --log=DEBUG

fmt:
	@black $(SRC_DIR)

zip:
	zip -r anexos_g06.zip matrices data solver output

clean:
	@echo "Cleaning..."
	@-cat .art/maid.ascii
	@rm $(REPORT).pdf
	@echo "...✓ done!"

